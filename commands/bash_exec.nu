# Execute a command in bash
def bash [
    ...command: string # The command to run
    ] {

    mut c = ""

    for $n: string in $command {
        $c = $"($c) ($n)"
    }

    ^bash -c $c
}